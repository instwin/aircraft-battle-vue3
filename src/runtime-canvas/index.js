import { createRenderer } from 'vue';
import { Container, Sprite, Texture, Text } from 'pixi.js';

// 自定义渲染器  自定义接口
const renderer = createRenderer({
  // 创建元素api
  createElement(type) {
    /**
     * @param type 通过组件传递过来的元素类型
     */

    let element;
    switch (type) {
      case 'container':
        element = new Container();
        break;

      case 'sprite':
        element = new Sprite();
        break;
      default:
        break;
    }

    return element;
  },
  // 插入元素
  insert(el, parent) {
    if (el && parent) {
      parent.addChild(el);
    }
  },
  // 匹配属性
  patchProp(el, key, prevValue, nextValue) {
    // console.log(key);
    switch (key) {
      // case 'x':
      //   el.x = nextValue;
      //   break;

      // case 'y':
      //   el.y = nextValue;
      //   break;

      // case 'interactive':
      //   el.interactive = nextValue;
      //   break;

      case 'texture':
        el.texture = Texture.from(nextValue);
        break;

      // 事件
      case 'onClick':
        el.on('pointertap', nextValue);

      default:
        el[key] = nextValue;
        break;
    }
  },
  // 获取父节点
  parentNode(node) {
    if (node) {
      return node.parent;
    }
  },
  // 移除元素
  remove(el) {
    if (el && el.parent) {
      el.parent.removeChild(el)
    }
  },
  // 创建文字对象
  createText(text) {
    return new Text(text);
  },
  // 无需实现
  nextSibling() { },
  createComment() { },
});

export function createApp(rootComponent) {
  return renderer.createApp(rootComponent);
}
