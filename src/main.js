import { createApp } from './runtime-canvas/index.js'
import App from './App.vue'
import { createRootContrainer } from './game/index.js'

// 消灭告警提示
window.console.warn = () => { }

// App(根组件)，createRootContrainer(根容器)
createApp(App).mount(createRootContrainer());
