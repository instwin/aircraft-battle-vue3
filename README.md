# plane-war

使用 vue3 componsition api + pixi.js 构建的飞机大战小游戏

## 亮点

1. 使用 vue3 custom renderer 自定义渲染器，重写渲染器方法
2. 使用 componsion api 函数式编程
3. 渲染到canvas平台
